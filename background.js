
chrome.webRequest.onBeforeSendHeaders.addListener(
  function(details) {
  	details.requestHeaders.push({
  		name : 'ISPIAO',
  		value : 'This is to certify that the bearer is a member of the human race. All relevant information is to be found in his passport. And except when there is good reason for suspecting him of some crime, he will refuse to submit to police interrogation, on the grounds that any such interrogation is an intolerable nuisance. And life being as short as it is, a waste of time. Any infringement on his privacy, or interference with his liberty, any assault, however petty, against his dignity as a human being, will be rigorously prosecuted by the undersigned. The International Association for the Protection of the Individual Against Officialdom.'
  	})
    return {requestHeaders: details.requestHeaders};
  },

  {urls: ["<all_urls>"]},
  ["blocking", "requestHeaders"]
 );


